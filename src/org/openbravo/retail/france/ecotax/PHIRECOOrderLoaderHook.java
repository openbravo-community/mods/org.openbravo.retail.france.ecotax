/*
 ************************************************************************************
 * Copyright (C) 2020-2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package org.openbravo.retail.france.ecotax;

import java.math.BigDecimal;
import java.util.function.Function;

import javax.enterprise.context.ApplicationScoped;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.france.ecotax.PhiecoTaxcategory;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.retail.posterminal.OrderLoaderHook;

@ApplicationScoped
public class PHIRECOOrderLoaderHook implements OrderLoaderHook {

  private enum PhiecoTaxCategoryType {
    DEEE("phiecoDeeenet", "phiecoDeeegross"), DEA("phiecoDeanet", "phiecoDeagross");

    private String netProperty;
    private String grossProperty;

    PhiecoTaxCategoryType(String netProperty, String grossProperty) {
      this.netProperty = netProperty;
      this.grossProperty = grossProperty;
    }
  }

  @Override
  public void exec(JSONObject jsonorder, Order order, ShipmentInOut shipment, Invoice invoice)
      throws Exception {
    for (OrderLine orderline : order.getOrderLineList()) {
      setEcoTaxInfo(orderline, "orderedQuantity");
    }

    if (invoice != null) {
      for (InvoiceLine invoiceLine : invoice.getInvoiceLineList()) {
        setEcoTaxInfo(invoiceLine, "invoicedQuantity");
      }
    }
  }

  private void setEcoTaxInfo(final BaseOBObject docLine, final String qtyPropertyName) {
    final Product product = (Product) docLine.get("product");
    if (product != null) {
      setEcoTaxAmounts(docLine, getAmount(product, product.getPhiecoDeeeamnt(),
          product.getPhiecoDeee(), Product::getPhiecoDeeeQty), PhiecoTaxCategoryType.DEEE,
          qtyPropertyName);
      setEcoTaxAmounts(docLine, getAmount(product, product.getPhiecoDeaamnt(),
          product.getPhiecoDea(), Product::getPhiecoDeaQty), PhiecoTaxCategoryType.DEA,
          qtyPropertyName);

      docLine.set("phiecoDeee", product.getPhiecoDeee());
      docLine.set("phiecoDea", product.getPhiecoDea());
    }
  }

  private void setEcoTaxAmounts(final BaseOBObject docLine, final BigDecimal amt,
      final PhiecoTaxCategoryType phiecoTaxCategoryType, final String qtyPropertyName) {
    // regular, code-based ecotaxes are used tax not included:
    // compute the net first, and then the gross from it
    docLine.set(phiecoTaxCategoryType.netProperty,
        amt.multiply((BigDecimal) docLine.get(qtyPropertyName)));
    BigDecimal taxAmount = ((BigDecimal) docLine.get(phiecoTaxCategoryType.netProperty))
        .multiply(((TaxRate) docLine.get("tax")).getRate())
        .divide(new BigDecimal("100"));
    docLine.set(phiecoTaxCategoryType.grossProperty,
        ((BigDecimal) docLine.get(phiecoTaxCategoryType.netProperty)).add(taxAmount));
  }

  private BigDecimal getAmount(Product product, BigDecimal amt, PhiecoTaxcategory phiecoTaxCategory,
      Function<Product, BigDecimal> getQty) {
    if (amt == null) {
      if (phiecoTaxCategory != null && phiecoTaxCategory.getTaxamt() != null) {
        return phiecoTaxCategory.getTaxamt().multiply(getQty.apply(product));
      } else {
        return BigDecimal.ZERO;
      }
    }
    return amt;
  }
}
